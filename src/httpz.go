package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type Watch struct {
	MessageType string `json:"messageType"`
	Execer      string `json:"execer"`
	Id          string `json:"id"`
	WatchId     string `json:"watchId"`
	WatchModel  string `json:"watchModel"`
	WatchColor  string `json:"watchColor"`
	WatchOwner  string `json:"watchOwner"`
}

type Reply struct {
	Id      string `json:"id"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func initHttpz() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/watches", createWatch).Methods("POST")

	log.Printf("INFO: http listening on %s", config.servicePort)

	// start server
	err := http.ListenAndServe(":"+config.servicePort, r)
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func createWatch(w http.ResponseWriter, r *http.Request) {
	// read body
	b, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: create watch request %s", string(b))

	// unmarshel json
	// obtain watch
	var watch Watch
	err := json.Unmarshal(b, &watch)
	if err != nil {
		log.Printf("ERROR: invalid json to unmarshal, %s", err.Error())
		return
	}

	// popup unique id
	uid := uid()
	watch.Id = uid

	// publish kafka message
	j, _ := json.Marshal(watch)
	kmsg := Kmsg{
		Topic: topicConfig.aplos,
		Uid:   uid,
		Msg:   string(j),
	}
	kchan <- kmsg

	// send response back to client
	reply := Reply{
		Id:      watch.Id,
		Code:    201,
		Message: "created",
	}
	response(w, reply, 201)
}

func response(w http.ResponseWriter, reply Reply, statusCode int) {
	j, _ := json.Marshal(reply)
	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json")

	log.Printf("write response: %s", string(j))
	io.WriteString(w, string(j))
}
