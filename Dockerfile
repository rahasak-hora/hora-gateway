FROM golang:1.9

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install dependencies
RUN go get gopkg.in/mgo.v2
RUN	go get github.com/gorilla/mux
RUN go get github.com/Shopify/sarama
RUN go get github.com/wvanbergen/kafka/consumergroup
RUN go get github.com/gorilla/handlers
RUN go get github.com/fln/nf9packet

# env
ENV SERVICE_NAME aplosapi
ENV SERVICE_MODE DEV
ENV SERVICE_PORT 8761
ENV KAFKA_TOPIC aplosapi
ENV KAFKA_CGROUP aplosapig
ENV KAFKA_ADDR localhost:9092
ENV ZOOKEEPER_ADDR localhost:2181
ENV APLOS_TOPIC aplos

# copy app
ADD . /app
WORKDIR /app

# build
RUN go build -o build/hora src/*.go

# server running port
EXPOSE 8761

# .logs volume
VOLUME ["/app/.logs"]

ENTRYPOINT ["/app/docker-entrypoint.sh"]
